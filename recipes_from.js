function startHorizontalScroll() {

  var rowOne = document.getElementById("row1");
  var rowOneCopy = document.getElementById("row1-copy");

  var left1 = 100;
  var left1_copy=100;



  var time_inter_val_r1 = setInterval(function () {

    rowOne.style.left = left1 - 1 + "%";
    rowOneCopy.style.left = left1_copy - 1 + "%";
    left1 = left1 - 1;
    left1_copy = left1_copy - 1;

    if (left1 == 0) {
      rowOneCopy.style.display = "inline-block";
      left1_copy = 100;
    }
    if(left1_copy==0){
         left1 = 100;
    }

  }, 150)
}
